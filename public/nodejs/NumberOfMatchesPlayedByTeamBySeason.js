const csvParse = require('csv-parser');
const fs = require('fs');

const seasons = {};
let check;
fs.createReadStream('../matches.csv')
  .pipe(csvParse())
  .on('data', (line) => {
    const year = line.season;
    const teamOne = line.team1;
    const teamTwo = line.team2;

    if (!seasons[teamOne]) {
      seasons[teamOne] = {};
    }
    if (!seasons[teamTwo]) {
      seasons[teamTwo] = {};
    }
    if (!seasons[teamOne][year] || !seasons[teamTwo][year]) {
      seasons[teamOne][year] = 0;
      seasons[teamTwo][year] = 0;
    }

    seasons[teamOne][year] += 1;
    seasons[teamTwo][year] += 1;
  })
  .on('end', () => {
    const key = Object.keys(seasons);
    for (let i = 2008; i <= 2017; i += 1) {
      for (let j = 0; j < key.length; j += 1) {
        check = seasons[key[j]];
        if (!check[String(i)]) {
          check[String(i)] = 0;
        }
      }
    }
    fs.writeFile('../NumberOfMatchesPlayedByTeamBySeason.json', JSON.stringify(seasons), () => {});
  });
