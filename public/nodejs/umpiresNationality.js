const csv = require('csv-parser');
const fs = require('fs');

const umpires = {};
fs.createReadStream('../umpires.csv')
  .pipe(csv())
  .on('data', (line) => {
    if (line.Nationality !== 'India') {
      if (!umpires[line.Nationality]) {
        umpires[line.Nationality] = 0;
      }
      umpires[line.Nationality] += 1;
    }
  })
  .on('end', () => {
    fs.writeFile('../umpiresNationality.json', JSON.stringify(umpires), () => {});
  });
