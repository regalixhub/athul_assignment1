fetch('NumberOfMatchesPlayedByTeamBySeason.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = [2008];
    for (let i = 0; year[i] < 2018; i += 1) {
      year.push(1 + year[i]);
    }
    const array = [];
    const pba = Object.keys(data);
    for (let j = 0; j < pba.length; j += 1) {
      array.push({
        name: pba[j],
        data: Object.values(data[pba[j]]),
      });
    }

    Highcharts.chart('container3', {
      chart: {
        type: 'bar',
      },
      title: {
        text: 'Matches Played by Team by Season',
      },
      xAxis: {
        title: {
          text: 'Years',
        },
        categories: year,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'IPL teams',
        },
      },
      legend: {
        reversed: true,
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: array,
    });
  });
